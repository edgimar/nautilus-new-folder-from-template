Description
===========

This Nautilus extension allows you to create template directories within the
`~/Templates/FolderTemplates` directory.  A "New Folder from Template"
menu-item will be added to the context menu when you right click on things in
Nautilus.  From this menu-item, you can choose a template directory structure
to be created.

The following three cases are supported -- right-clicking on:

  * empty background => template copied to subfolder of current folder
  * a directory => template copied to subfolder of selected dir
  * a file => template copied to subfolder of file's parent

The main motivation for creating this extension is to be able to create entire
directory structures from templates, just as you can create new individual files
from templates.  This is convenient if you have several projects which require
the same kinds of files.  A LaTeX project, for example, might be well suited to
this, since one document may require multiple files -- preamble, bibliography,
main document, etc.


Installation
============

To install, simply run the `install.sh` script included in this repository.

This script checks that required dependencies are installed, and copies the `nautilus-new-folder-from-template.py` file to the `~/.local/share/nautilus-python/extensions` directory, as well as copying over language translation files to the same directory.


About
=====

Author/License
--------------

- License: GPLv3
- Author: Mark Edgington ([bitbucket site](https://bitbucket.org/edgimar/nautilus-new-folder-from-template))

Contributing
------------

Patches / pull-requests are welcome.
