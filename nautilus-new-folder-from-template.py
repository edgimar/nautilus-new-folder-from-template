#!/usr/bin/env python3
# Copyright 2014 Mark Edgington
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""nautilus-new-folder-from-template.py -- add menu items to copy template
folders as subfolders of the current or selected nautilus folder.

To install, put this file in the ~/.local/share/nautilus-python/extensions
directory.

2014-01-06, Mark Edgington

"""
import os
import urllib.parse
import shutil
import re
import time
import subprocess

import locale
import gettext

import gi
gi.require_version('GConf', '2.0')

from gi.repository import Nautilus, GObject, GConf


# localize language if translation is available
default_locale = locale.getdefaultlocale()[0]
# localization folder must contain subfolders / files like:
#     <loc_folder>/en/LC_MESSAGES/messages.mo
#
# This .mo file must be generated from a .po file, by using the 'msgfmt'
# executable from the commandline:  for example, run 'msgfmt messages.po'
#
localization_folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'locales-nautilus-new-folder-from-template')
try:
    trans = gettext.translation('messages', localization_folder, languages=[default_locale])
    trans.install()
except IOError:
    # default locale not found / translation doesn't exist
    # define _ function to use default behavior
    def _(message): return message


class DirectoryTemplateExtension(Nautilus.MenuProvider, GObject.GObject):
    def __init__(self):
        self.client = GConf.Client.get_default()
        try:
            xdg_tpl_dir = subprocess.check_output(['/usr/bin/xdg-user-dir', 'TEMPLATES']).strip()
        except subprocess.CalledProcessError:
            xdg_tpl_dir = os.path.expanduser('~/' + _('Templates'))

        self.templates_parent_dir = os.path.join(xdg_tpl_dir.decode('utf-8'), _("FolderTemplates"))

        # create the template-folders parent dir if it doesn't already exist
        subprocess.check_output(['mkdir', '-p', self.templates_parent_dir])



    def copy_template_dir(self, tpl_foldername, dir_to_create_subfolder_in,
                          dest_subfolder_name=None):
        """Copy the specified template folder to the specified parent directory.

        All of the contents of the template directory will be copied
        (recursively) into a folder inside of *dir_to_create_subfolder_in*.

        *dir_to_create_subfolder_in* is the absolute path of the directory into
        which the subfolder should be placed.

        *tpl_foldername* is only the name of the template directory, not the
        absolute path to it.  Its absolute path is formed by prefixing the
        folder-name with the *self.templates_parent_dir*.

        If *dest_subfolder_name* is specified, it will be used as the
        destination into which *tpl_foldername*'s contents are copied into.
        Otherwise, its contents are copied into a folder having the same name.

        """
        tpl_folder_abspath = os.path.abspath(os.path.join(self.templates_parent_dir, tpl_foldername))

        if dest_subfolder_name is None:
            dest_subfolder_name = tpl_foldername

        dest_subfolder_name = self.substitute_name(dest_subfolder_name)

        try:
            shutil.copytree(tpl_folder_abspath, os.path.join(dir_to_create_subfolder_in, dest_subfolder_name))
        except OSError as e:
            # couldn't copy folder (permissions issue or dest folder already exists)
            print("Unable to create template folder: " + str(e))

    def substitute_name(self, fname):
        """Replace any parts of *fname* with other content (e.g. timestamp)

        If *fname* contains somewhere within it the special string
        "%date[...]" (where ... represents a date/time format), then this
        will be replaced by the correspondingly formatted date/time.

        For example, if *fname* is "myfile-%date[%Y-%m-%d].txt", then it might
        be modified so that it this function returns "myfile-2014-01-01.txt",
        depending on the current time/date.

        """
        try:
            matches = re.search(".*(%date\[.+\]).*", fname).groups()
        except AttributeError:
            # there were no matches
            matches = []

        for match in matches:
            timedate_pattern = re.search("%date\[(.+)\]", match).groups()[0]
            datestr = time.strftime(timedate_pattern)
            fname = fname.replace(match, datestr)

        return fname

    def menu_activate_cb(self, menu, tpl_foldername, target_parent_dir):
        target_subfolder = tpl_foldername

        self.copy_template_dir(tpl_foldername, target_parent_dir, target_subfolder)

    def add_submenu_items(self, submenu, itemlist, dir_to_create_subfolder_in):
        for i,item in enumerate(itemlist):
            sub_menuitem = Nautilus.MenuItem(name='DirectoryTemplateExtension::%s' % i,
                                             label=item,
                                             tip='',
                                             icon='')
            submenu.append_item(sub_menuitem)

            # pass 'item' to the activate_cb method when this item is selected
            sub_menuitem.connect('activate', self.menu_activate_cb, item, dir_to_create_subfolder_in)


    def get_list_of_template_dirs(self):
        "Return a list of dirs contained in *self.templates_parent_dir*"
        return os.listdir(self.templates_parent_dir)

    def get_file_items(self, window, files):
        if len(files) != 1:
            return

        f = files[0]

        # the 'file' URI scheme is used for files and for directories
        if f.get_uri_scheme() != 'file':
            return

        if f.is_directory():
            # it is already a directory, so get the absolute path to it
            target_parent_dir = urllib.parse.unquote(f.get_uri()[7:]) # directory that this extension was called on/in
        else:
            # it is only a file, so get the absolute path of f's parent directory
            target_parent_dir = urllib.parse.unquote(f.get_parent_uri()[7:]) # directory that this extension was called on/in

        top_menuitem = Nautilus.MenuItem(name='DirectoryTemplateExtension::NFFT',
                                         label=_('New Folder from Template'),
                                         tip='',
                                         icon='')

        submenu = Nautilus.Menu()
        top_menuitem.set_submenu(submenu)

        template_dir_choices = self.get_list_of_template_dirs()
        self.add_submenu_items(submenu, template_dir_choices,
                               dir_to_create_subfolder_in=target_parent_dir)

        return top_menuitem,

    def get_background_items(self, window, file_obj):
        """Function called by Nautilus to get menus.

        *file_obj* is a Nautilus VFS file object representing the current dir.

        """
        # the 'file' URI scheme is used for files and for directories
        if file_obj.get_uri_scheme() != 'file':
            return

        f = file_obj

        # it is already a directory, so get the absolute path to it
        target_parent_dir = urllib.parse.unquote(f.get_uri()[7:]) # directory that this extension was called on/in

        top_menuitem = Nautilus.MenuItem(name='DirectoryTemplateExtension::NFFT',
                                         label=_('New Folder from Template'),
                                         tip='',
                                         icon='')

        submenu = Nautilus.Menu()
        top_menuitem.set_submenu(submenu)

        template_dir_choices = self.get_list_of_template_dirs()
        self.add_submenu_items(submenu, template_dir_choices,
                               dir_to_create_subfolder_in=target_parent_dir)

        return top_menuitem,

